package com.flipkart.Header;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.flipkart.Base.TestBase;
import com.flipkart.Pages.HeaderPage;
import com.flipkart.Pages.ProductListingPage;

public class SearchTest extends TestBase{

	
	@BeforeSuite
	public void setUP() throws IOException{
		TestBase.init();
		
	}
	
	@Test
	public void searctTest(String product){
		HeaderPage hp= PageFactory.initElements(driver, HeaderPage.class);
		hp.serachProduct(product);
		
		ProductListingPage pl= PageFactory.initElements(driver, ProductListingPage.class);
		boolean res= pl.verifySearchedProduct(product);
		
		Assert.assertTrue(res, "Product serach for product : "+ product + " failed.");
	}
	
	
}
